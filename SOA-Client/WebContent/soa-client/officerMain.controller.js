sap.ui
		.controller(
				"soa-client.officerMain",
				{

					onInit : function() {
						document.title = "RMS renewal online web application";
						var oShell = sap.ui.getCore().byId("mainShell");
						var osearchLayout = new sap.ui.commons.layout.MatrixLayout(
								{
									width : "auto"
								});
						osearchLayout.createRow(searchText);
						var searchText = new sap.ui.commons.TextView(
								{
									text : "please press the search button to search for the list of renewal requests:"
								});
						oShell.addContent(searchText);
						var searchButton = new sap.ui.commons.Button({
							text : "Search",
							width : "100px",
							press : function(oEvent) {
							}
						})
						osearchLayout.createRow(searchButton);
						oShell.addContent(osearchLayout);
					},

					workSetItemSelected : function(oEvent) {
						var oShell = sap.ui.getCore().byId("mainShell");
						switch (oEvent.mParameters.key) {
						case "home":
							oShell.destroyContent();
							var osearchLayout = new sap.ui.commons.layout.MatrixLayout(
									{
										width : "auto"
									});
							osearchLayout.createRow(searchText);
							var searchText = new sap.ui.commons.TextView(
									{
										text : "please press the search button to search for the list of renewal requests:"
									});
							oShell.addContent(searchText);
							var searchButton = new sap.ui.commons.Button({
								text : "Search",
								width : "100px",
								press : function(oEvent) {

								}
							})
							osearchLayout.createRow(searchButton);
							oShell.addContent(osearchLayout);
							break;
						case "renewList":
							oShell.destroyContent();
							var oTable = new sap.ui.table.Table("landscapeTable", {
								visibleRowCount : 15,
								navigationMode : sap.ui.table.NavigationMode.Paginator
							});
							oShell.addContent(oTable);
							break;
						case "renewItem":
							oShell.destroyContent();
							break;
						}
					}

				});
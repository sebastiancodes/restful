sap.ui
		.jsview(
				"soa-client.officerMain",
				{

					/**
					 * Specifies the Controller belonging to this View. In the
					 * case that it is not implemented, or that "null" is
					 * returned, this View does not have a Controller.
					 * 
					 * @memberOf soa-client.officerMain
					 */
					getControllerName : function() {
						return "soa-client.officerMain";
					},

					/**
					 * Is initially called once after the Controller has been
					 * instantiated. It is the place where the UI is
					 * constructed. Since the Controller is given to this
					 * method, its event handlers can be attached right away.
					 * 
					 * @memberOf soa-client.officerMain
					 */
					createContent : function(oController) {
						var oShell = new sap.ui.ux3.Shell("mainShell",{
							appTitle : "RMS renewal online web application",
							id : "main-shell",
							showPane : true,
							showLogoutButton : false,
							showFeederTool : false,
							showSearchTool : false
						});
						var homeNavItem = new sap.ui.ux3.NavigationItem({key:"home",text:"Home"});
						oShell.addWorksetItem(homeNavItem);
						var renewalReqListNavItem = new sap.ui.ux3.NavigationItem({key:"renewList",text:"Renewal List"});
						oShell.addWorksetItem(renewalReqListNavItem);
						var renewalReqNavItem = new sap.ui.ux3.NavigationItem({key:"renewItem",text:"Renewal item"});
						oShell.addWorksetItem(renewalReqNavItem);
						oShell.attachWorksetItemSelected(oController.workSetItemSelected);
						return oShell;
					}
				});

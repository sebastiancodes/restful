<xsl:stylesheet
     xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
     xmlns:xhtml="http://www.w3.org/1999/xhtml"
     version="1.1">
<xsl:output indent="yes"/>
<!-- category gets POSTCODE or FUEL-TYPE -->
<xsl:variable name="category" select="normalize-space(xhtml:html/xhtml:body/xhtml:div/xhtml:div/xhtml:div/xhtml:table/xhtml:thead/xhtml:tr[1]/xhtml:th[1])"/>
  <xsl:template match="/">
    <root>
      <xsl:text>&#xa;</xsl:text>
      <xsl:apply-templates select="xhtml:html/xhtml:body/xhtml:div/xhtml:div/xhtml:div/xhtml:table/xhtml:tbody/xhtml:tr"/>
    </root>
  </xsl:template>

  
  <xsl:template match="xhtml:html/xhtml:body/xhtml:div/xhtml:div/xhtml:div/xhtml:table/xhtml:tbody/xhtml:tr">

      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:value-of select="translate($category,' ','-')"/>
      
      <xsl:text disable-output-escaping="yes"> name="</xsl:text>
      <xsl:variable name="categoryName" select="xhtml:th"/>
      <xsl:value-of select="normalize-space($categoryName)"/>
      <xsl:text disable-output-escaping="yes">"&gt;</xsl:text>
      <xsl:text>&#xa;   </xsl:text>
      
      <total><xsl:value-of select="xhtml:td[1]"/></total>
      <passenger_vehicles><xsl:value-of select="xhtml:td[2]"/></passenger_vehicles>
      <off_road_vehicles><xsl:value-of select="xhtml:td[3]"/></off_road_vehicles>
      <people_movers><xsl:value-of select="xhtml:td[4]"/></people_movers>
      <small_buses><xsl:value-of select="xhtml:td[5]"/></small_buses>
      <buses><xsl:value-of select="xhtml:td[6]"/></buses>
      <mobile_homes><xsl:value-of select="xhtml:td[7]"/></mobile_homes>
      <motorcycles><xsl:value-of select="xhtml:td[8]"/></motorcycles>
      <scooters><xsl:value-of select="xhtml:td[9]"/></scooters>
      <light_trucks><xsl:value-of select="xhtml:td[10]"/></light_trucks>
      <heavy_trucks><xsl:value-of select="xhtml:td[1]"/></heavy_trucks>
      <prime_movers><xsl:value-of select="xhtml:td[12]"/></prime_movers>
      <light_plants><xsl:value-of select="xhtml:td[13]"/></light_plants>
      <heavy_plants><xsl:value-of select="xhtml:td[14]"/></heavy_plants>
      <small_trailers><xsl:value-of select="xhtml:td[15]"/></small_trailers>
      <trailers><xsl:value-of select="xhtml:td[16]"/></trailers>
      
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:value-of select="translate($category,' ','-')"/>
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <xsl:text>&#xa;</xsl:text>
  </xsl:template>
  
</xsl:stylesheet>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	version="1.0">

	<xsl:param name="fName" />
	<xsl:param name="lName" />
	<xsl:param name="regoNum" />

	<xsl:template match="Customers">
		<CENTER>
			<H2>
				<xsl:value-of select="$fName" />
				<xsl:value-of select="$lName" />
				<xsl:value-of select="$regoNum" />
			</H2>
			<xsl:apply-templates select="Entry/CarDetails[RegistrationNumber=$regoNum]" />
		</CENTER>
	</xsl:template>

	<xsl:template match="Entry/CarDetails">
		<xsl:variable name="lastName" select="../Driver/LastName" />
		<xsl:variable name="firstName" select="../Driver/FirstName" />
		<answer>
			<xsl:if test="$firstName=$fName and $lastName=$lName">
				<xsl:value-of select="ManufacturedYear" />
			</xsl:if>
		</answer>
	</xsl:template>

</xsl:stylesheet>


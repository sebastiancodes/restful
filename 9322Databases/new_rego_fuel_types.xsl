<xsl:stylesheet
     xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
     version="1.0">

<xsl:param name="fuel"/>     
<xsl:param name="sort"/> <!-- ascending or descending-->
     
<xsl:template match="root">
  <root>
    <xsl:choose>
      <xsl:when test="$fuel = ''">
        <xsl:if test="$sort='ascending'"> <!--sort ascending-->
          <xsl:apply-templates select="FUEL-TYPE">
            <xsl:sort data-type="text" order="ascending" select="./@name"/>
          </xsl:apply-templates>
        </xsl:if>
        <xsl:if test="$sort='descending'"> <!--sort descending-->
          <xsl:apply-templates select="FUEL-TYPE">
            <xsl:sort data-type="text" order="descending" select="./@name"/>
          </xsl:apply-templates>
        </xsl:if>
        <xsl:if test="$sort=''"> <!-- no sorting-->
          <xsl:apply-templates select="FUEL-TYPE"/>
        </xsl:if>
      </xsl:when>
      
      <xsl:otherwise>
        <xsl:if test="$sort='ascending'"> <!-- sort ascending-->
          <xsl:apply-templates select="FUEL-TYPE[@name=$fuel]">
            <xsl:sort data-type="text" order="ascending" select="./@name"/>
          </xsl:apply-templates>
        </xsl:if>
        <xsl:if test="$sort='descending'"><!--sort descending-->
          <xsl:apply-templates select="FUEL-TYPE[@name=$fuel]">
            <xsl:sort data-type="text" order="descending" select="./@name"/>
          </xsl:apply-templates>
        </xsl:if>
        <xsl:if test="$sort=''"> <!-- no sorting-->
          <xsl:apply-templates select="FUEL-TYPE[@name=$fuel]"/>
        </xsl:if>
      </xsl:otherwise>
      
    </xsl:choose>
  </root>
</xsl:template>

<xsl:template match="FUEL-TYPE">
  <xsl:variable name="attri" select="./@name"/>
  <xsl:text disable-output-escaping="yes">&lt;FUEL-TYPE name="</xsl:text>
  <xsl:value-of select="$attri"/>
  <xsl:text disable-output-escaping="yes">"&gt;</xsl:text>
  <xsl:apply-templates select="total"/>
  <xsl:text disable-output-escaping="yes">&lt;/FUEL-TYPE&gt;</xsl:text>
  <br/>
</xsl:template>

<xsl:template match="total">
  <total><xsl:value-of select="."/></total>
  <passenger_vehicles><xsl:value-of select="../passenger_vehicles"/></passenger_vehicles>
  <off_road_vehicles><xsl:value-of select="../off_road_vehicles"/></off_road_vehicles>
  <people_movers><xsl:value-of select="../people_movers"/></people_movers>
  <small_buses><xsl:value-of select="../small_buses"/></small_buses>
  <buses><xsl:value-of select="../buses"/></buses>
  <mobile_homes><xsl:value-of select="../mobile_homes"/></mobile_homes>
  <motorcycles><xsl:value-of select="../motorcycles"/></motorcycles>
  <scooters><xsl:value-of select="../scooters"/></scooters>
  <light_trucks><xsl:value-of select="../light_trucks"/></light_trucks>
  <heavy_trucks><xsl:value-of select="../heavy_trucks"/></heavy_trucks>
  <prime_movers><xsl:value-of select="../prime_movers"/></prime_movers>
  <light_plants><xsl:value-of select="../light_plants"/></light_plants>
  <heavy_plants><xsl:value-of select="../heavy_plants"/></heavy_plants>
  <small_trailers><xsl:value-of select="../small_trailers"/></small_trailers>
  <trailers><xsl:value-of select="../trailers"/></trailers>
</xsl:template>

</xsl:stylesheet>


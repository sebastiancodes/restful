package au.edu.unsw.soacourse.RMSservice;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class RenewalNotice {
	private String _nid;
	private String _rid;
	private String status;
	// should be created, cancelled, requested, under-review, accepted, 
	// rejected, archived... or finalised
	
	
	
	// from here is just getters and setters
	public String get_nid() {
		return _nid;
	}
	public void set_nid(String _nid) {
		this._nid = _nid;
	}
	public String get_rid() {
		return _rid;
	}
	public void set_rid(String _rid) {
		this._rid = _rid;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	
	
	
	
}

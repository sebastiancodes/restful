package au.edu.unsw.soacourse.RMSservice;

public class Payment {
	private String _pid;
	private String _nid;
	private double amount;
	private String credit_card_details;
	private String paid_date;
	
	
	// from here is just getters and setters
	public String get_pid() {
		return _pid;
	}
	public void set_pid(String _pid) {
		this._pid = _pid;
	}
	public String get_nid() {
		return _nid;
	}
	public void set_nid(String _nid) {
		this._nid = _nid;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	public String getCredit_card_details() {
		return credit_card_details;
	}
	public void setCredit_card_details(String credit_card_details) {
		this.credit_card_details = credit_card_details;
	}
	public String getPaid_date() {
		return paid_date;
	}
	public void setPaid_date(String paid_date) {
		this.paid_date = paid_date;
	}
	
}

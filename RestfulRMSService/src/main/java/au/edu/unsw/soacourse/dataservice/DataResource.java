package au.edu.unsw.soacourse.dataservice;

import java.io.File;
import java.io.FileOutputStream;
import java.util.logging.Logger;
import java.util.Scanner;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;

//http://www.rgagnon.com/javadetails/java-0407.html
@Path("/registrationDS")
public class DataResource {
	// Allows to insert contextual objects into the class, 
	// e.g. ServletContext, Request, Response, UriInfo
	@Context
	UriInfo uriInfo;
	@Context
	Request request;
		
	String dir = System.getProperty("catalina.home");
	String directoryName = dir + "/webapps/ROOT/9322Databases";
	
	String fuelHtml = directoryName + "/table126.html";
	String fuelXml = directoryName + "/table126.xml";
	
	String postcodeHtml = directoryName + "/table127.html";
	String postcodeXml = directoryName + "/table127.xml";
	
	//strings for xsl paths
	String LFT = directoryName + "/list_fuel_types.xsl";
	String LVT = directoryName + "/list_vehicle_types.xsl";
	String NRFT = directoryName + "/new_rego_fuel_types.xsl";
	String NRP = directoryName + "/new_rego_postcode.xsl";
	String convert = directoryName + "/convertTables.xsl";
	
	String out = directoryName + "/out.xml";
	
	//when using postman, printing info in logger
	// and going to http://docs.brightcove.com/en/video-cloud/player-management/guides/postman.html
	// might be useful for debugging
	static Logger logger = Logger.getLogger(DataResource.class.getName());

	// list all vehicle types
	@GET
	@Path("list_vehicle_types")
	@Produces(MediaType.APPLICATION_XML)
	public Response listVehicleTypes() {
		logger.info("DataResource: GET on list_vehicle_types");
		generateFuelXML();
		return applyXslToXml(LVT, fuelXml, "", "", "");
	}
	
	//sort by for above
	@GET
	@Path("list_vehicle_types/sort_by={sortRegex}")
	@Produces(MediaType.APPLICATION_XML)
	public Response listVehicleTypeSort(@PathParam("sortRegex") String sortRegex) {
		generateFuelXML();
		if (sortRegex.equals("ascending")) {
			return applyXslToXml(LVT, fuelXml, "", "", "ascending");
		} else if (sortRegex.equals("descending")) {
			return applyXslToXml(LVT, fuelXml, "", "", "descending");
		} else {
			return Response.status(400).entity("cannot sort on " + sortRegex 
					+ ". You can only sort by ascending or descending.").build();
		}
	}
	
	// list all fuel types
	@GET
	@Path("list_fuel_types")
	@Produces(MediaType.APPLICATION_XML)
	public Response listFuelTypes() {
		logger.info("DataResource: GET on list_fuel_types");
		generateFuelXML();
		return applyXslToXml(LFT, fuelXml, "", "", "");
	}
	
	//sort by for above
	@GET
	@Path("list_fuel_types/sort_by={sortRegex}") //@FormParam("id") String id
	@Produces(MediaType.APPLICATION_XML)
	public Response listFuelTypeSort(@PathParam("sortRegex") String sortRegex) {
		generateFuelXML();
		if (sortRegex.equals("ascending")) {
			return applyXslToXml(LFT, fuelXml, "", "", "ascending");
		} else if (sortRegex.equals("descending")) {
			return applyXslToXml(LFT, fuelXml, "", "", "descending");
		} else {
			return Response.status(400).entity("cannot sort on " + sortRegex 
					+ ". You can only sort by ascending or descending.").build();
		}
	}
	
	//list all regos
	@GET
	@Path("new_rego_fuel_type")
	@Produces(MediaType.APPLICATION_XML)
	public Response newRegoFuelType() {
		generateFuelXML();
		return applyXslToXml(NRFT, fuelXml, "", "", "");
	}
	
	//sort by for above
	@GET
	@Path("new_rego_fuel_type/sort_by={sortRegex}") //@FormParam("id") String id
	@Produces(MediaType.APPLICATION_XML)
	public Response newRegoFuelTypeSort(@PathParam("sortRegex") String sortRegex) {
		generateFuelXML();
		if (sortRegex.equals("ascending")) {
			return applyXslToXml(NRFT, fuelXml, "", "", "ascending");
		} else if (sortRegex.equals("descending")) {
			return applyXslToXml(NRFT, fuelXml, "", "", "descending");
		} else {
			return Response.status(400).entity("cannot sort on " + sortRegex 
					+ ". You can only sort by ascending or descending.").build();
		}
	}
	
	//also might not return correct stuff for Petrol & Compressed Natural Gas
	//get info for this fuel type
	// for Electric/Petrol, url is Eletric|Petrol
	@GET
	@Path("new_rego_fuel_type/{fuelRegex}")
	@Produces(MediaType.APPLICATION_XML)
	public Response newRegoFuelTypeParam(@PathParam("fuelRegex") String fuelRegex) {
		generateFuelXML();
		fuelRegex = fuelRegex.replace("_", " ");
		fuelRegex = fuelRegex.replace("|", "/");
		return applyXslToXml(NRFT, fuelXml, "fuel", fuelRegex, "");
	}
	//^not sure if this needs sorting
	
	
	//list all regos
	@GET
	@Path("new_rego_postcode")
	@Produces(MediaType.APPLICATION_XML)
	public Response newRegoPostcode() {
		generatePostcodeXML();
		return applyXslToXml(NRP, postcodeXml, "", "", "");
	}
	
	//sort for above
	@GET
	@Path("new_rego_postcode/sort_by={sortRegex}")
	@Produces(MediaType.APPLICATION_XML)
	public Response newRegoPostcodeSort(@PathParam("sortRegex") String sortRegex) {
		generatePostcodeXML();
		if (sortRegex.equals("ascending")) {
			return applyXslToXml(NRP, postcodeXml, "", "", "ascending");
		} else if (sortRegex.equals("descending")) {
			return applyXslToXml(NRP, postcodeXml, "", "", "descending");
		} else {
			return Response.status(400).entity("cannot sort on " + sortRegex 
					+ ". You can only sort by ascending or descending.").build();
		}
	}
	
	//get postcode with this regex
	@GET
	@Path("new_rego_postcode/{postRegex}")
	@Produces(MediaType.APPLICATION_XML)
	public Response newRegoPostcodeParam(@PathParam("postRegex") String postRegex) {
		generatePostcodeXML();
		return applyXslToXml(NRP, postcodeXml, "postcode", postRegex, "ascending");
	}
	
	//sort for above
	@GET
	@Path("new_rego_postcode/{postRegex}/sort_by={sortRegex}")
	@Produces(MediaType.APPLICATION_XML)
	public Response newRegoPostcodeParamSort(@PathParam("postRegex") String postRegex, @PathParam("sortRegex") String sortRegex) {
		generatePostcodeXML();
		if (sortRegex.equals("ascending")) {
			return applyXslToXml(NRP, postcodeXml, "postcode", postRegex, "ascending");
		} else if (sortRegex.equals("descending")) {
			return applyXslToXml(NRP, postcodeXml, "postcode", postRegex, "descending");
		}  else {
			return Response.status(400).entity("cannot sort on " + sortRegex 
					+ ". You can only sort by ascending or descending.").build();
		}
	}
	
	//returns response with output of query 
	private Response applyXslToXml(String xsl, String xml, String matchType, String matchValue, String sort) {
		try {
			TransformerFactory tFactory = TransformerFactory.newInstance();

			Transformer transformer = tFactory
					.newTransformer(new javax.xml.transform.stream.StreamSource(
							xsl));

			//applying parameters if necessary
			if (!matchType.isEmpty()) {
				transformer.setParameter(matchType, matchValue);
				logger.info("matchType is " + matchType + ". matchValue is " + matchValue);
			}
			if (!sort.isEmpty()) {
				transformer.setParameter("sort", sort);
				logger.info("sort is " + sort);
			}
			
			transformer.transform(new javax.xml.transform.stream.StreamSource(
					xml), new javax.xml.transform.stream.StreamResult(
					new FileOutputStream(out)));
			
			String output = "";
			Scanner sc = new Scanner(new File(out));
			while (sc.hasNext()) {
				String line = sc.nextLine();
				output = output + line + "\n";
			}
			sc.close();
			logger.info("output is: " + output);
			
			return Response.ok(output).build();
		} catch (Exception e) {
			return Response.status(400).entity("could not perform query" + xsl + " on database " + xml).build();
//			e.printStackTrace();
		}
	}
	
	private void applyXslToHtml(String xsl, String html, String outputFileName) {
		try {
			//using net.sf.saxon.TransformerFactoryImpl because we want to parse xslt 2.0
			TransformerFactory tFactory = new net.sf.saxon.TransformerFactoryImpl();
//			TransformerFactory tFactory = TransformerFactory.newInstance();
			logger.info("about to run xslt " + xsl + " on html file " + html);
			Transformer transformer = tFactory
					.newTransformer(new javax.xml.transform.stream.StreamSource(
							xsl));
			
			transformer.transform(new javax.xml.transform.stream.StreamSource(
					html), new javax.xml.transform.stream.StreamResult(
					new FileOutputStream(outputFileName)));
			
			logger.info("Finished converting " + html + ", output at " + outputFileName);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	// if fuel xml doesn't exist, we convert the html to xml
	private void generateFuelXML() {
		if (!(new File(fuelXml).exists())) {
			logger.info("DataResourse: generating fuel xml");
			applyXslToHtml(convert, fuelHtml, fuelXml);
		}
	}
	
	// if postcode xml doesn't exist, we convert the html to xml
	private void generatePostcodeXML() {
		if (!(new File(postcodeXml).exists())) {
			logger.info("DataResourse: generating postcode xml");
			applyXslToHtml(convert, postcodeHtml, postcodeXml);
		}
	}
	
}
<xsl:stylesheet
     xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
     version="1.0">
   
<xsl:param name="sort"/> <!-- ascending or descending--> 
     
<xsl:template match="root">
  <root>
    <xsl:if test="$sort='ascending'"> <!--sort ascending-->
      <xsl:for-each select="FUEL-TYPE[1]/*">
        <xsl:sort data-type="text" order="ascending" select="name()"/>
        <VEHICLE-TYPE><xsl:value-of select="name(.)"/></VEHICLE-TYPE>
      </xsl:for-each>
    </xsl:if>
    
    <xsl:if test="$sort='descending'"> <!--sort ascending-->
      <xsl:for-each select="FUEL-TYPE[1]/*">
        <xsl:sort data-type="text" order="descending" select="name()"/>
        <VEHICLE-TYPE><xsl:value-of select="name(.)"/></VEHICLE-TYPE>
      </xsl:for-each>
    </xsl:if>
    
    <xsl:if test="$sort=''"> <!-- no sorting -->
      <xsl:for-each select="FUEL-TYPE[1]/*">
        <VEHICLE-TYPE><xsl:value-of select="name(.)"/></VEHICLE-TYPE>
      </xsl:for-each>
    </xsl:if>
  </root>
</xsl:template>

</xsl:stylesheet>










package au.edu.unsw.soacourse.RMSservice;

import java.util.HashMap;

public enum PaymentsDAO {
	instance;

	private HashMap<String, Payment> payments = new HashMap<String, Payment>();
	
	public HashMap<String, Payment> getPayments() {
		return payments;
	}
	
}

package au.edu.unsw.soacourse.RMSservice;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.xml.sax.Attributes;
import org.xml.sax.ContentHandler;
import org.xml.sax.InputSource;
import org.xml.sax.Locator;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;

//parses content to make RenewalNotice from Database.xml
public class DatabaseParser_CarRegos implements ContentHandler {
	Logger logger = Logger.getLogger(this.getClass().getName());
	
	
	//IMPORTANT: will the parser deal with attributes within the xml tags? 
	//SEEMS to be fine
	private enum TagType {
		Registrations, Entry, _rid, RegistrationNumber, Driver, LastName, 
		FirstName, DriversLicenseNo, Email, RegistrationValidTill, unidentified
	}
	
	/*We'll use these to keep track of which element is being processed at the moment*/
	private TagType openingTag = TagType.unidentified, closingTag = TagType.unidentified;
	
	/*This is a string buffer collecting characters*/
	private StringBuilder builder;
	
	
	/*publication will be used to build a CarRego from the content in the tags*/
	private CarRego rego;
	
	/*List of publications that will be produced by parsing the XML file*/
	private ArrayList<CarRego> regos;
	
	public DatabaseParser_CarRegos(InputStream xmlFile) throws ParsingFailedException{
		regos = new ArrayList<CarRego>();
		try {
			InputSource xmlSource = new InputSource(xmlFile);
	        XMLReader parser = XMLReaderFactory.createXMLReader();
	        
	        /*Give reference to a ContentHandler implementation*/
	        parser.setContentHandler(this);
	        parser.parse(xmlSource);  					
	   } catch(Exception e){
		   logger.log(Level.SEVERE,"Failed to process XML file: {0} ",e);
		   throw new ParsingFailedException(e); 
	   }  
	}

	public ArrayList<CarRego> getCarRegos() {
		return regos;
	}


	@Override
	public void characters(char[] ch, int start, int length)
			throws SAXException {
		
//		logger.info("Adding characters to the buffer");
		builder.append(new String(ch,start,length));
	}

	@Override
	public void endDocument() throws SAXException {
		System.out.println("Parsing Ended");

	}

	@Override
	public void endElement(String uri, String localName, String qName)
			throws SAXException {
		logger.info("End Element :"+qName);
		
		/* Now we try to find what the closing element is*/
		
		try {
			closingTag = TagType.valueOf(TagType.class,qName);
		} catch(Exception e){
			logger.warning("Could not find tagType for tag "+qName);
			closingTag = TagType.unidentified;
		}
		
		String content = builder.toString();
		
		/* Do actions based on the closing tag type*/
		switch(closingTag){
		case DriversLicenseNo:
			rego.setLicenseNum(content);
			logger.info("DriversLicenseNo : " + content);
			break;
		case Email:
			rego.setEmail(content);
			logger.info("Email : " + content);
			break;
		case Entry:
			regos.add(rego);
			logger.info("Added new carRego to list with rid=" + rego.get_rid() + ", firstName=" + rego.getFirstName());
			break;
		case FirstName:
			rego.setFirstName(content);
			logger.info("FirstName : " + content);
			break;
		case LastName:
			rego.setLastName(content);
			logger.info("LastName : " + content);
			break;
		case _rid:
			rego.set_rid(content);
			logger.info("_rid : " + content);
			break;
		case RegistrationNumber:
			logger.info("RegistrationNumber : " + content);
			rego.setRegoNum(content);
			break;
		case RegistrationValidTill:
			logger.info("registrationValidTill : " + content);
			rego.setRegoValidTill(content);
			break;
		case Registrations:
			logger.info("Finished parsing all notices");
			break;
		case unidentified:
			logger.warning("Closing tag not idenitifed");
			break;
		default:
			break;
		}
		// need to set the stuff to null if the tags doesn't exist?
		// tried for edition for bookParser and seems okay.
//		logger.info("Done parsing "+qName);
		
	}

	@Override
	public void endPrefixMapping(String prefix) throws SAXException {
		// TODO Auto-generated method stub

	}

	@Override
	public void ignorableWhitespace(char[] ch, int start, int length)
			throws SAXException {
		// TODO Auto-generated method stub

	}

	@Override
	public void processingInstruction(String target, String data)
			throws SAXException {
		// TODO Auto-generated method stub

	}

	@Override
	public void setDocumentLocator(Locator locator) {
		// TODO Auto-generated method stub

	}

	@Override
	public void skippedEntity(String name) throws SAXException {
		// TODO Auto-generated method stub

	}

	@Override
	public void startDocument() throws SAXException {
		logger.info("Parsing started");

	}

	@Override
	public void startElement(String uri, String localName, String qName,
			Attributes atts) throws SAXException {
//		logger.info("Started Element : "+qName);
		
		try{
			openingTag = TagType.valueOf(TagType.class,qName);
		}catch(Exception e){
			logger.warning("Could not find tagType for tag "+qName);
			openingTag = TagType.unidentified;
		}
		
		/*
		 * Create a new bean when opening tag for book element is encountered  
		 */
		if(openingTag.toString().equals("Entry")){
			logger.info("Created new car rego object");
			rego = new CarRego();
		}
		
		/* Create a new character buffer to hold contents of the element */
//		logger.info("Created new buffer");
		builder = new StringBuilder();

	}

	@Override
	public void startPrefixMapping(String prefix, String uri)
			throws SAXException {
		// TODO Auto-generated method stub

	}
	
	
}

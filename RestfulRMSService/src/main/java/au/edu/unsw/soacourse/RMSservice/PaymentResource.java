package au.edu.unsw.soacourse.RMSservice;
//this is just copied from RestfulBookService from lab04
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.logging.Logger;
import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.apache.commons.mail.DefaultAuthenticator;
import org.apache.commons.mail.Email;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.SimpleEmail;
import org.json.simple.JSONObject;


@Path("/resources/payments")
public class PaymentResource {
	// Allows to insert contextual objects into the class, 
	// e.g. ServletContext, Request, Response, UriInfo
	@Context
	UriInfo uriInfo;
	@Context
	Request request;
	
	String driverKey = "driver456";
	String officerKey = "officer123";
	
	//when using postman, printing info in logger
	// and going to http://docs.brightcove.com/en/video-cloud/player-management/guides/postman.html
	// might be useful for debugging

	static Logger logger = Logger.getLogger(PaymentResource.class.getName());

	/**
	 * The officer should indicate how much the renewal fee is.
	 * @param payment
	 * @return response contains a URI of the new payment and its location.
	 * @throws IOException
	 */
	@SuppressWarnings("unchecked")
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response makeNewPayment(@HeaderParam("Key") String key, String input) throws IOException {
		logger.info("Payment resource: got a POST request to make new Payment");

		JSONObject msg = new JSONObject();
		
		if (input.isEmpty()) {
			msg.put("msg", "You have to ender a price for the payment.");
			return Response.status(400).entity(msg).build();
		}
		
		//processing input.
		String[] inputs = input.replace("{", "").replace("}", "").replace("\"", "").split(",");
		String inputNid = inputs[0].split(":")[1];
		String price = inputs[1].split(":")[1];
		logger.info("PaymentResource POST: price from input JSON is " + price
				+ ". input nid is " + inputNid);
		
		//only the officer should be able to access create payments
		if (key == null || !key.equals(officerKey)) {
			msg.put("msg", "You do not have the correct key to access this.");
			return Response.status(401).entity(msg).build();
		}
		
		RenewalNotice notice = RenewalNoticesDAO.instance.getAllNotices().get(inputNid);
		if (notice == null) {
			msg.put("msg", "This payment with nid " + inputNid + "doesn't have an associated notice.");
			return Response.status(400).entity(msg).build();
		}
		
		if (!notice.getStatus().equals("accepted")) {
			msg.put("msg", "A payment can only be created if its associated renewal notice (nid is " + inputNid + ") was accepted"
					+ " was accepted, but this one was " + notice.getStatus() + ".");
			return Response.status(400).entity(msg).build();
		}
		
		Payment payment = new Payment();
		String pid = UUID.randomUUID().toString();
		payment.set_pid(pid);
		payment.set_nid(inputNid);
		//the following could give an error
		payment.setAmount(Double.parseDouble(price));
		String uriPayment = uriInfo.getBaseUri() + "resources/payments/" + pid;
		JSONObject paymentDetails = new JSONObject();
		paymentDetails.put("href", uriPayment);
		paymentDetails.put("pid", pid);
		
		CarRego rego = CarRegoDAO.instance.getCarRegos().get(notice.get_rid());
		if (rego == null) {
			msg.put("msg", "There is no car registration associated with this payment");
			return Response.status(400).entity(msg).build();
		}
		
		//email the driver telling them to enter their details for payment
		String driverLinkNotice = "http://localhost:8080/SOA-Client/driver.html?_nid=" + notice.get_nid()
				+ "&price=" + price;
		sendEmail(rego.getEmail(), driverLinkNotice, price);
		
		PaymentsDAO.instance.getPayments().put(pid, payment);		
		return Response.ok(paymentDetails, MediaType.APPLICATION_JSON).build();
	}
	
	/**
	 * @param pid
	 * @return Response with details about payment with this pid
	 */
	@SuppressWarnings("unchecked")
	@GET
	@Path("{pid}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getPayment(@HeaderParam("Key") String key, @PathParam("pid") String pid) {
		logger.info("Payment resource: got a GET request for " + pid);
		
		JSONObject msg = new JSONObject();
		
		//if it isn't the officer or driver
		if (key == null || (!key.equals(officerKey) && !key.equals(driverKey))) {
			msg.put("msg", "You do not have the correct key to access this.");
			return Response.status(401).entity(msg).build();
		}
		
		Payment payment = PaymentsDAO.instance.getPayments().get(pid);
		
		if (payment == null) {
			msg.put("msg", "This payment with pid " + pid + " couldn't be found in the list of payments");
			return Response.status(400).entity(msg).build();
		}
		
		// Payment resource shouldn't be given to the driver if their
		// renewal notice wasn't accepted
		RenewalNotice notice = RenewalNoticesDAO.instance.getAllNotices().get(payment.get_nid());
		if (notice == null) {
			msg.put("msg", "This payment with pid " + pid + " and nid " + payment.get_nid() + " couldn't be found"
					+ "in the list of renewal notices");
			return Response.status(400).entity(msg).build();
		}
		if (!notice.getStatus().equals("accepted")) {
			msg.put("msg", "The notice for this payment is " + notice.getStatus()
					+ " and is not \"accepted\"");
			return Response.status(409).entity(msg).build();
		}
		
		notice.setStatus("finalised");
		
		//TODO: may need to add more values to the JSON
		JSONObject paymentJSON = new JSONObject();
		paymentJSON.put("_pid", payment.get_pid());
		paymentJSON.put("_nid", payment.get_nid());
		paymentJSON.put("amount", payment.getAmount());
		paymentJSON.put("credit_card_details", payment.getCredit_card_details());
		paymentJSON.put("paid_date", payment.getPaid_date());
		
		return Response.ok(paymentJSON, MediaType.APPLICATION_JSON).build();
	}
	
	/**
	 * Only drivers initiate a HTTP PUT. This is called when a driver
	 * enters their credit_card_details
	 * @param pid
	 * @param payment
	 * @return
	 */
	//TODO: we'll need the 
	@SuppressWarnings("unchecked")
	@PUT
	@Path("{nid}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response putPayment(@HeaderParam("Key") String key, @PathParam("nid") String nid, String input) {
		logger.info("Payment resource: got a PUT request for " + nid);
		
		logger.info("input is " + input);
		String credit_card_details = "";
		String[] inputs = input.replace("{", "").replace("}", "").replace("\"", "").replace(" ", "").split(":");
		if (inputs[0].equals("credit_card_details")) {
			credit_card_details = inputs[1];
			logger.info("credit_card_details is " + credit_card_details);
		}
		
		JSONObject msg = new JSONObject();
		if (key == null || !key.equals(driverKey)) {
			msg.put("msg", "Only drivers initiate a HTTP PUT");
			return Response.status(401).entity(msg).build();
		}
		
		RenewalNotice notice = RenewalNoticesDAO.instance.getAllNotices().get(nid);
		if (notice == null) {
			msg.put("msg", "This payment with renewal notice's nid " + nid + " couldn't be found"
					+ "in the list of renewal notices");
			return Response.status(404).entity(msg).build();
		}
		
		Payment payment = null;
		for (Payment p: PaymentsDAO.instance.getPayments().values()) {
			if (p.get_nid().equals(nid)) {
				payment = p;
				logger.info("Payment found, has nid " + payment.get_nid());
				break;
			}
		}
		
		if (payment == null) {
			msg.put("msg", "This payment with renewal notice's nid " + nid + " couldn't be found"
					+ "in the list of renewal notices");
			return Response.status(404).entity(msg).build();
		}
		
		//setting the paid date
		// if the one in the database doesn't have a paid date already
		// we set that field to today's date
		if (payment.getPaid_date() == null || payment.getPaid_date().isEmpty()) {
			Date todayDate = Calendar.getInstance().getTime();
			String date = new SimpleDateFormat("dd/MM/yyyy").format(todayDate);
			payment.setPaid_date(date);
		}
		
		payment.setCredit_card_details(credit_card_details);
		PaymentsDAO.instance.getPayments().put(payment.get_pid(), payment);
		return Response.ok().build();
	}
	
	private void sendEmail(String emailAddress, String noticeLink, String price) {
		Email email = new SimpleEmail();
		
		email.setSmtpPort(465);
		email.setHostName("smtp.gmail.com");
		email.setAuthenticator(new DefaultAuthenticator("9322dummy@gmail.com", "hopethisworks"));
		email.setSSLOnConnect(true);
		try {
			email.setFrom("9322dummy@gmail.com");
			email.setSubject("RMS Renewal notice");
			email.setMsg("You have a renewal notice for your vehicle. This will cost: $" + price + 
					". Please click the link and enter your credit card details "+ noticeLink
					+ ".");
			email.addTo(emailAddress);
			logger.info("sent email to " + emailAddress);
			email.send();
		} catch (EmailException e) {
			logger.severe("Could not send email to " + emailAddress + " with link " + noticeLink);
		}
		
	}
	
}
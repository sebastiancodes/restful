<xsl:stylesheet
     xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
     version="1.0">
 
<xsl:param name="sort"/> <!-- ascending or descending--> 
 
<xsl:template match="root">
  <root>
    <xsl:if test="$sort='ascending'"> <!--sort ascending-->
      <xsl:apply-templates select="FUEL-TYPE">
        <xsl:sort data-type="text" order="ascending" select="./@name"/>
      </xsl:apply-templates>
    </xsl:if>
    <xsl:if test="$sort='descending'"> <!--sort ascending-->
      <xsl:apply-templates select="FUEL-TYPE">
        <xsl:sort data-type="text" order="descending" select="./@name"/>
      </xsl:apply-templates>
    </xsl:if>
    <xsl:if test="$sort=''"> <!--sort ascending-->
      <xsl:apply-templates select="FUEL-TYPE"/>
    </xsl:if>
  </root>
</xsl:template>

<xsl:template match="FUEL-TYPE">
  <FUEL-TYPE><xsl:value-of select="translate(./@name,' ','_')"/></FUEL-TYPE>
</xsl:template>

</xsl:stylesheet>



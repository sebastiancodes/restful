package au.edu.unsw.soacourse.RMSservice;

public class CarRego {
	private String _rid;
	private String regoNum;
	
	private String lastName;
	private String firstName;
	private String licenseNum;
	private String email;
	
	private String regoValidTill;

	//TODO: decide parameters later
	public CarRego() {
		
	}
	
	// from here is just getters and setters
	
	public String get_rid() {
		return _rid;
	}

	public void set_rid(String _rid) {
		this._rid = _rid;
	}

	public String getRegoNum() {
		return regoNum;
	}

	public void setRegoNum(String regoNum) {
		this.regoNum = regoNum;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLicenseNum() {
		return licenseNum;
	}

	public void setLicenseNum(String licenseNum) {
		this.licenseNum = licenseNum;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getRegoValidTill() {
		return regoValidTill;
	}

	public void setRegoValidTill(String regoValidTill) {
		this.regoValidTill = regoValidTill;
	}
	
	
}

package au.edu.unsw.soacourse.RMSservice;

public class ParsingFailedException extends Exception {

	public ParsingFailedException(Exception e) {
		super(e);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
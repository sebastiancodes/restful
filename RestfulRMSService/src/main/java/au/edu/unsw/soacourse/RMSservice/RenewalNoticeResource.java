package au.edu.unsw.soacourse.RMSservice;
//this is just copied from RestfulBookService from lab04
import java.io.IOException;







import org.apache.commons.mail.DefaultAuthenticator;
import org.apache.commons.mail.Email;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.SimpleEmail;
//make sure the json-simple1.1.1.jar is on the buildpath or else there'll be errors
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.util.logging.Logger;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;


@Path("/resources/renewalNotice")
public class RenewalNoticeResource {
	// Allows to insert contextual objects into the class, 
	// e.g. ServletContext, Request, Response, UriInfo
	@Context
	UriInfo uriInfo;
	@Context
	Request request;
	
	String driverKey = "driver456";
	String officerKey = "officer123";
	
	//when using postman, printing info in logger
	// and going to http://docs.brightcove.com/en/video-cloud/player-management/guides/postman.html
	// might be useful for debugging
	static Logger logger = Logger.getLogger(RenewalNoticeResource.class.getName());

	
	@SuppressWarnings("unchecked")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllRenewalNotices(@HeaderParam("Key") String key) throws IOException, ParsingFailedException {
		logger.info("RenewalNoticeResource: GET");

		//only the officer should be able to access the notices
		if (key == null || !key.equals(officerKey)) {
			JSONObject msg = new JSONObject();
			msg.put("msg", "You do not have the correct key to access this.");
			return Response.status(401).entity(msg).build();
		}
		
		List<JSONObject> noticesDetails = new ArrayList<JSONObject>();
		
		if (CarRegoDAO.instance.getCarRegos().isEmpty()) {
			CarRegoDAO.instance.initaliseCarRegoDAO();
		}
		
		logger.info("There are " + CarRegoDAO.instance.getCarRegos().size() + "car registrations");
		for (CarRego carRego: CarRegoDAO.instance.getCarRegos().values()) {
			logger.info(carRego.get_rid() + " " + carRego.getFirstName() + " " + carRego.getLastName() + " " + carRego.getLicenseNum());
		}
		
		for (RenewalNotice notice: RenewalNoticesDAO.instance.getAllNotices().values()) {
			CarRego rego = CarRegoDAO.instance.getCarRegos().get(notice.get_rid());
			String uriNotice = uriInfo.getBaseUri() + "resources/renewalNotice/" + notice.get_nid();
			
			JSONObject noticeJSON = new JSONObject();
			noticeJSON.put("_nid", notice.get_nid());
			noticeJSON.put("_rid", notice.get_rid());
			// status should all be "created", but i do that in DatabaseParser_RenewalNotices
			noticeJSON.put("status", notice.getStatus());
			noticeJSON.put("href", uriNotice);
			noticeJSON.put("email", rego.getEmail());
			logger.info("made noticeJSON: " + noticeJSON);
			
			noticesDetails.add(noticeJSON);
		}
		
		JSONArray ListJSON = new JSONArray();
		ListJSON.addAll(noticesDetails);
		logger.info("GET ListJSON is " + ListJSON.toString());
		return Response.ok(ListJSON, MediaType.APPLICATION_JSON).build();
	}
	
	@SuppressWarnings("unchecked")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	public Response createRenewalNotices(@HeaderParam("Key") String key) throws IOException, ParsingFailedException {
		logger.info("we've reached the createRenewalNotices method");
		
		//only the officer should be able to access the notices
		if (key == null || !key.equals(officerKey)) {
			JSONObject msg = new JSONObject();
			msg.put("msg", "You do not have the correct key to access this.");
			return Response.status(401).entity(msg).build();
		}
		
		List<RenewalNotice> notices = RenewalNoticesDAO.instance.createNotices();

		List<JSONObject> noticesDetails = new ArrayList<JSONObject>();
		
		if (CarRegoDAO.instance.getCarRegos().isEmpty()) {
			CarRegoDAO.instance.initaliseCarRegoDAO();
		}
		
		logger.info("There are " + CarRegoDAO.instance.getCarRegos().size() + "car registrations");
		for (CarRego carRego: CarRegoDAO.instance.getCarRegos().values()) {
			logger.info(carRego.get_rid() + " " + carRego.getFirstName() + " " + carRego.getLastName() + " " + carRego.getLicenseNum());
		}
		logger.info("There are " + notices.size() + "newly created notices");
		
		for (RenewalNotice notice: notices) {
			CarRego rego = CarRegoDAO.instance.getCarRegos().get(notice.get_rid());
			String uriNotice = uriInfo.getBaseUri() + "resources/renewalNotice/" + notice.get_nid();
			
			JSONObject noticeJSON = new JSONObject();
			noticeJSON.put("_nid", notice.get_nid());
			noticeJSON.put("_rid", notice.get_rid());
			// status should all be "created", but i do that in DatabaseParser_RenewalNotices
			noticeJSON.put("status", notice.getStatus());
			noticeJSON.put("href", uriNotice);
			noticeJSON.put("email", rego.getEmail());
			String driverLinkNotice = "http://localhost:8080/SOA-Client/driver.html?_nid=" + notice.get_nid();
			sendEmail(rego.getEmail(), driverLinkNotice);
			logger.info("made noticeJSON: " + noticeJSON);
			
			noticesDetails.add(noticeJSON);
		}
		
		JSONArray ListJSON = new JSONArray();
		ListJSON.addAll(noticesDetails);
		logger.info("ListJSON is " + ListJSON.toString());
		return Response.ok(ListJSON, MediaType.APPLICATION_JSON).build();
	}
		
	@SuppressWarnings("unchecked")
	@GET
	@Path("{nid}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getRenewalNotice(@HeaderParam("Key") String key, @PathParam("nid") String nid) {
		JSONObject msg = new JSONObject();
		
		//only the officer or the driver can see notices
		if (key == null || (!key.equals(officerKey) && !key.equals(driverKey))) {
			msg.put("msg", "You do not have the correct key to access this.");
			return Response.status(401).entity(msg).build();
		}
		
		RenewalNotice notice = RenewalNoticesDAO.instance.getAllNotices().get(nid);
		if (notice == null) {
			msg.put("msg", "GET: Renewal notice with " + nid +  " not found");
			return Response.status(400).entity(msg).build();
		}
		
		CarRego rego = CarRegoDAO.instance.getCarRegos().get(notice.get_rid());
		JSONObject noticeJSON = new JSONObject();
		noticeJSON.put("lastName", rego.getLastName());
		noticeJSON.put("firstName", rego.getFirstName());
		noticeJSON.put("licenseNumber", rego.getLicenseNum());
		noticeJSON.put("registrationNumber", rego.getRegoNum());
		noticeJSON.put("_nid", notice.get_nid());
		noticeJSON.put("_rid", notice.get_rid());
		// status should all be "created", but i do that in DatabaseParser_RenewalNotices
		noticeJSON.put("status", notice.getStatus());
		
		return Response.ok(noticeJSON, MediaType.APPLICATION_JSON).build();
	}
	
	
	
	/* TODO:
	 * As the workflows from both sides progress, there will be multiple HTTP PUT on a 
	 * renewal notice resource to update various parts of the resource. Of course, in 
	 * particular, the status field of a renewal notice resource is maintained by HTTP 
	 * PUT to push the workflow along.
	 * 
	 */
	@SuppressWarnings("unchecked")
	//WORKS - for some stuff, haven't finished checking
	@PUT
	@Path("{nid}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response putRenewalNotice(@HeaderParam("Key") String key, @PathParam("nid") String nid, String input) {
		logger.info("reached PUT with nid " + nid + ", input " + input + ", key " + key);
		RenewalNotice notice = RenewalNoticesDAO.instance.getAllNotices().get(nid);
		JSONObject msg = new JSONObject();
		
		String[] inputs = input.split(":");
		String status = inputs[1].replace("\"", "").replace("}", "").trim(); 
		
		logger.info("status is " + status);
		
		if (notice == null || status.equals("created")) {
			// this notice doesn't exist yet.
			// but we shouldn't add it in because notices should only be generated by the system
			msg.put("msg", "Notices can only be generated by the system.");
			return Response.status(406).entity(msg).build();
		}
		
		String prevStatus = notice.getStatus();
		if (prevStatus.equals("archived")) {
			msg.put("msg", "This notice has already been archived.");
			return Response.status(406).entity(msg).build();
		}
		
		if (prevStatus.equals("finalised")) {
			msg.put("msg", "This notice has already been finalised.");
			return Response.status(406).entity(msg).build();
		}
		
		//if key hasn't been set
		if (key == null) {
			msg.put("msg", "You do not have the correct key to access this.");
			return Response.status(401).entity(msg).build();
		}
		
		//request is from driver side. drivers can only set these statuses:
		//requested, cancelled, archived
		if (key.equals(driverKey)) {
			if (status.equals(prevStatus)) {
				return Response.ok().build();
			}
			if (prevStatus.equals("under-review")) {
				msg.put("msg", "This notice is already under review.");
				return Response.status(409).entity(msg).build();
			} 
			if (status.equals("under-review") 
					|| status.equals("accepted")
					|| status.equals("rejected")) {
				msg.put("msg", "Only the system can review, accept or reject a notice.");
				return Response.status(409).entity(msg).build();
			}
			// notices can only be archived by the driver
			// if it was previously rejected or cancelled
			if ((status.equals("archived") && !prevStatus.equals("cancelled")) ||
					(status.equals("archived") && !prevStatus.equals("rejected"))) {
				msg.put("msg", "Notices can only be archived if they were cancelled or rejected");
				return Response.status(409).entity(msg).build();
			}	
			notice.setStatus(status);
			return Response.status(200).build();
					
		//the PUT request is from the officer. officers can only set these statuses:
		//under-review, accepted, rejected
		} else if (key.equals(officerKey)){
			if (status.equals("cancelled") 
				|| status.equals("requested")
				|| status.equals("archived")) {
				msg.put("msg", "Only the driver can cancel, request or archive a notice.");
				return Response.status(409).entity(msg).build();
			} 
			if (status.equals("under-review") && !prevStatus.equals("requested")) {
				msg.put("msg", "A renewal notice can only be \"under review\" if a driver has requested it.");
				return Response.status(409).entity(msg).build();
			}
			if (status.equals(prevStatus)) {
				return Response.ok().build();
			}
			if ((status.equals("accepted") && !prevStatus.equals("under-review")) ||
					status.equals("rejected") && !prevStatus.equals("under-review")) {
				msg.put("msg", "A renewal notice can only be accepted or rejected if it was previously \"under review\".");
				return Response.status(409).entity(msg).build();
			}
			//email the driver telling them they can archive their renewal notice
			if (status.equals("rejected")) {
				CarRego rego = CarRegoDAO.instance.getCarRegos().get(notice.get_rid());
				if (rego == null) {
					msg.put("msg", "There is no car registration associated with this payment");
					return Response.status(400).entity(msg).build();
				}
				String driverLinkNotice = "http://localhost:8080/SOA-Client/driver.html?_nid=" + notice.get_nid();
				sendRejectedEmail(rego.getEmail(), driverLinkNotice);
			}
			notice.setStatus(status);
			logger.info("Successfully updated the status");
			return Response.ok().build();
		} else {
			msg.put("msg", "You do not have the correct key to access this.");
			return Response.status(401).entity(msg).build();
		}
			
	}

	@SuppressWarnings("unchecked")
	@DELETE
	@Path("{nid}")
	@Consumes(MediaType.APPLICATION_JSON)
	// doesn't actually need any JSON as an input since the nid from the path is a unique identifier
	public Response deleteRenewalNotice(@HeaderParam("Key") String key, @PathParam("nid") String nid) {
		JSONObject msg = new JSONObject();
		
		//only the driver can archive a notice
		if (key == null || !key.equals(driverKey)) {
			
			msg.put("msg", "You do not have the correct key to access this.");
			return Response.status(401).entity(msg).build();
		}
		
		RenewalNotice notice = RenewalNoticesDAO.instance.getAllNotices().get(nid);
		if (notice == null) {
			msg.put("msg", "Renewal notice with " + nid +  " not found");
			return Response.status(404).entity(msg).build();
		}
		
		if (!notice.getStatus().equals("rejected") 
				&& !notice.getStatus().equals("cancelled")) {
			msg.put("msg", "A notice can only be archived if it was rejected or"
					+ "cancelled.");
			return Response.status(409).entity(msg).build();
		} 
		
		notice.setStatus("archived");
		RenewalNoticesDAO.instance.getAllNotices().put(notice.get_nid(), notice);
		return Response.ok().build();
	}
	
	private void sendEmail(String emailAddress, String noticeLink) {
		Email email = new SimpleEmail();
		
		email.setSmtpPort(465);
		email.setHostName("smtp.gmail.com");
		email.setAuthenticator(new DefaultAuthenticator("9322dummy@gmail.com", "hopethisworks"));
		email.setSSLOnConnect(true);
		try {
			email.setFrom("9322dummy@gmail.com");
			email.setSubject("RMS Renewal notice");
			email.setMsg("You have a renewal notice for your vehicle. Here is a link with the details: " + noticeLink
					+ " You can go there any time to check the status of the renewal.");
			email.addTo(emailAddress);
			logger.info("sent email to " + emailAddress);
			email.send();
		} catch (EmailException e) {
			logger.severe("Could not send email to " + emailAddress + " with link " + noticeLink);
		}
		
	}
	
	private void sendRejectedEmail(String emailAddress, String noticeLink) {
		Email email = new SimpleEmail();
		
		email.setSmtpPort(465);
		email.setHostName("smtp.gmail.com");
		email.setAuthenticator(new DefaultAuthenticator("9322dummy@gmail.com", "hopethisworks"));
		email.setSSLOnConnect(true);
		try {
			email.setFrom("9322dummy@gmail.com");
			email.setSubject("RMS Renewal notice");
			email.setMsg("Your renewal notice for your vehicle was reject because it didn't pass the"
					+ "green slip and pink slip checks. Please go to this link to archive your notice:  " + noticeLink
					+ " .");
			email.addTo(emailAddress);
			logger.info("sent email to " + emailAddress);
			email.send();
		} catch (EmailException e) {
			logger.severe("Could not send email to " + emailAddress + " with link " + noticeLink);
		}
		
	}
	
}
package au.edu.unsw.soacourse.RMSservice;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Logger;

public enum RenewalNoticesDAO {
	instance;
	
	private HashMap<String, RenewalNotice> allNotices = new HashMap<String, RenewalNotice>();
	
	static Logger logger = Logger.getLogger(RenewalNoticeResource.class.getName());
	ArrayList<RenewalNotice> notices;
	
	
	/**
	 * @return list of the newly generated notices (from the database, based on RegistrationValidTill
	 * @throws IOException
	 * @throws ParsingFailedException
	 */
	// reads through Database.xml of drivers
	// and creates a notice for each person where applicable
	//also adds these to the HashMap
	public ArrayList<RenewalNotice> createNotices() throws IOException, ParsingFailedException {
		notices = new ArrayList<RenewalNotice>();
		
		// create a directory under catalina home for the database stuff
		String dir = System.getProperty("catalina.home");
		String directoryName = dir + "/webapps/ROOT/9322Databases";
				
		String databasePath = directoryName + "/Database.xml";
		
		//use parser to make notices from Database.xml
		InputStream database = new FileInputStream(new File(databasePath));
		DatabaseParser_RenewalNotices parser = new DatabaseParser_RenewalNotices(database);
		notices = parser.getNotices();
		logger.info("in renewalNoticesDAO - ran parser and got " + notices.size() + " new notices");
		
		//add these newly generated notices to the map
		for (RenewalNotice notice: notices) {
			allNotices.put(notice.get_nid(), notice);
		}
		
		//return the newly generated notices
		return notices;
	}
	
	public HashMap<String, RenewalNotice> getAllNotices() {
		return allNotices;
		
	}
	
}

package au.edu.unsw.soacourse.RMSservice;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.xml.sax.Attributes;
import org.xml.sax.ContentHandler;
import org.xml.sax.InputSource;
import org.xml.sax.Locator;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;

//parses content to make RenewalNotice from Database.xml
public class DatabaseParser_RenewalNotices implements ContentHandler {
	Logger logger = Logger.getLogger(this.getClass().getName());
	
	
	//IMPORTANT: will the parser deal with attributes within the xml tags? 
	//SEEMS to be fine
	private enum TagType {
		Registrations, Entry, _rid, RegistrationNumber, Driver, LastName, 
		FirstName, DriversLicenseNo, Email, RegistrationValidTill, unidentified
	}
	
	/*We'll use these to keep track of which element is being processed at the moment*/
	private TagType openingTag = TagType.unidentified, closingTag = TagType.unidentified;
	
	/*This is a string buffer collecting characters*/
	private StringBuilder builder;
	
	
	/*publication will be used to build a PublicationBean from the content in the tags*/
	private RenewalNotice notice;
	private String registrationValidTill;
	
	/*List of publications that will be produced by parsing the XML file*/
	private ArrayList<RenewalNotice> notices;
	
	public DatabaseParser_RenewalNotices(InputStream xmlFile) throws ParsingFailedException{
		notices = new ArrayList<RenewalNotice>();
		try {
			InputSource xmlSource = new InputSource(xmlFile);
	        XMLReader parser = XMLReaderFactory.createXMLReader();
	        
	        /*Give reference to a ContentHandler implementation*/
	        parser.setContentHandler(this);
	        parser.parse(xmlSource);  					
	   } catch(Exception e){
		   logger.log(Level.SEVERE,"Failed to process XML file: {0} ",e);
		   throw new ParsingFailedException(e); 
	   }  
	}

	public ArrayList<RenewalNotice> getNotices() {
		return notices;
	}


	@Override
	public void characters(char[] ch, int start, int length)
			throws SAXException {
		
		logger.info("Adding characters to the buffer");
		builder.append(new String(ch,start,length));
	}

	@Override
	public void endDocument() throws SAXException {
		System.out.println("Parsing Ended");

	}

	@Override
	public void endElement(String uri, String localName, String qName)
			throws SAXException {
		logger.info("End Element :"+qName);
		
		/* Now we try to find what the closing element is*/
		
		try {
			closingTag = TagType.valueOf(TagType.class,qName);
		} catch(Exception e){
			logger.warning("Could not find tagType for tag "+qName);
			closingTag = TagType.unidentified;
		}
		
		String content = builder.toString();
		
		/* Do actions based on the closing tag type*/
		switch(closingTag){
		case Entry:
			//check if date in registrationValidTill - 1 month <= today's date <= date in registrationValidTill
			//is in format dd/mm/yyyy
			
			String[] validTillDate = registrationValidTill.split("/");
			int day = Integer.parseInt(validTillDate[0]);
			// - 1 because the month starts from 0
			int month = Integer.parseInt(validTillDate[1]) - 1;
			int year = Integer.parseInt(validTillDate[2]);
			Calendar validTillCal = Calendar.getInstance();
			validTillCal.set(year, month, day);
			
			Calendar validTillMonthBefore = (Calendar) validTillCal.clone();
			//TODO: make sure it gets the right month
			validTillMonthBefore.add(Calendar.MONTH, -1);
			
			Calendar today = Calendar.getInstance();
			
			logger.info("validTillMonthBefore is " + validTillMonthBefore.get(Calendar.YEAR) + "-" + validTillMonthBefore.get(Calendar.MONTH) + "-" + validTillMonthBefore.get(Calendar.DAY_OF_MONTH));
			logger.info("today is " + today.get(Calendar.YEAR) + "-" + today.get(Calendar.MONTH) + "-" + today.get(Calendar.DAY_OF_MONTH));
			logger.info("validTillCal is " + validTillCal.get(Calendar.YEAR) + "-" + validTillCal.get(Calendar.MONTH) + "-" + validTillCal.get(Calendar.DAY_OF_MONTH));
			
			
			//only add this renewal notice if it's in the correct time frame
			if (today.after(validTillMonthBefore) && today.before(validTillCal)) {
				UUID uuId = UUID.randomUUID();
				notice.set_nid(uuId.toString());
				notice.setStatus("created");
				notices.add(notice);
				logger.info("\n!!!!!!!Added new renewal notice to list - rid:" + notice.get_rid() 
						+ ", nid:" + notice.get_nid());
			}
			
			break;
		case _rid:
			notice.set_rid(content);
//			logger.info("_rid : " + content);
			break;
		case RegistrationValidTill:
//			logger.info("registrationValidTill : " + content);
			registrationValidTill = content;
			break;
		case Registrations:
			logger.info("Finished parsing all notices");
			break;
		case unidentified:
			logger.warning("Closing tag not idenitifed");
			break;
		default:
			break;
		}
		// need to set the stuff to null if the tags doesn't exist?
		// tried for edition for bookParser and seems okay.
//		logger.info("Done parsing "+qName);
		
	}

	@Override
	public void endPrefixMapping(String prefix) throws SAXException {
		// TODO Auto-generated method stub

	}

	@Override
	public void ignorableWhitespace(char[] ch, int start, int length)
			throws SAXException {
		// TODO Auto-generated method stub

	}

	@Override
	public void processingInstruction(String target, String data)
			throws SAXException {
		// TODO Auto-generated method stub

	}

	@Override
	public void setDocumentLocator(Locator locator) {
		// TODO Auto-generated method stub

	}

	@Override
	public void skippedEntity(String name) throws SAXException {
		// TODO Auto-generated method stub

	}

	@Override
	public void startDocument() throws SAXException {
		logger.info("Parsing started");

	}

	@Override
	public void startElement(String uri, String localName, String qName,
			Attributes atts) throws SAXException {
//		logger.info("Started Element : "+qName);
		
		try{
			openingTag = TagType.valueOf(TagType.class,qName);
		}catch(Exception e){
			logger.warning("Could not find tagType for tag "+qName);
			openingTag = TagType.unidentified;
		}
		
		/*
		 * Create a new bean when opening tag for book element is encountered  
		 */
		if(openingTag.toString().equals("Entry")){
			logger.info("Created new renewal notice object");
			notice = new RenewalNotice();
		}
		
		/* Create a new character buffer to hold contents of the element */
		logger.info("Created new buffer");
		builder = new StringBuilder();

	}

	@Override
	public void startPrefixMapping(String prefix, String uri)
			throws SAXException {
		// TODO Auto-generated method stub

	}
	
	
}

package au.edu.unsw.soacourse.RMSservice;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.HashMap;

public enum CarRegoDAO {
	instance;

	private HashMap<String, CarRego> regos = new HashMap<String, CarRego>();

	public void initaliseCarRegoDAO() throws FileNotFoundException, ParsingFailedException {
		// create a directory under catalina home for the database stuff
		String dir = System.getProperty("catalina.home");
		String directoryName = dir + "/webapps/ROOT/9322Databases";

		String databasePath = directoryName + "/Database.xml";

		// use parser to make notices from Database.xml
		InputStream database = new FileInputStream(new File(databasePath));
		DatabaseParser_CarRegos parser = new DatabaseParser_CarRegos(database);
		for (CarRego carRego: parser.getCarRegos()) {
			regos.put(carRego.get_rid(), carRego);
		}
	}
	
	public HashMap<String, CarRego> getCarRegos() {
		return regos;
	}
}

package au.edu.unsw.soacourse.RMSservice;
//this is just copied from RestfulBookService from lab04
import java.io.FileNotFoundException;
import java.util.logging.Logger;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.json.simple.JSONObject;

//TODO: make sure all safety and idempotency properties are satisfied
@Path("/resources/carRego")
public class CarRegoResource {
	// Allows to insert contextual objects into the class, 
	// e.g. ServletContext, Request, Response, UriInfo
	@Context
	UriInfo uriInfo;
	@Context
	Request request;
	
	String driverKey = "driver456";
	String officerKey = "officer123";
	
	//when using postman, printing info in logger, might be useful for debugging

	static Logger logger = Logger.getLogger(CarRegoResource.class.getName());

//	// Return the list of all car registrations
	//this isn't actually required from the assignment spec
//	@GET
//	@Produces(MediaType.APPLICATION_JSON)
//	public List<CarRego> getAllCarRegos() {
//		List<CarRego> regos = new ArrayList<CarRego>();
//		regos.addAll(CarRegoDAO.instance.getCarRegos().values());
//		
//		return regos; 
//	}
	
	// 
	/**
	 * @param rid
	 * @return the car registration with the rid provided
	 * @throws ParsingFailedException 
	 * @throws FileNotFoundException 
	 */
	//THIS WORKS
	@SuppressWarnings("unchecked")
	@GET
	@Path("{rid}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getCarRego(@HeaderParam("Key") String key, @PathParam("rid") String rid) throws FileNotFoundException, ParsingFailedException {
		logger.info("GET car with rid " + rid);
		
		JSONObject msg = new JSONObject();
		
		//only the officer should be able to access the car registrations
		if (key == null || !key.equals(officerKey)) {
			msg.put("msg", "You do not have the correct key to access this.");
			return Response.status(401).entity(msg).build();
		}
		
		if (CarRegoDAO.instance.getCarRegos().isEmpty()) {
			CarRegoDAO.instance.initaliseCarRegoDAO();
		}
		CarRego rego = CarRegoDAO.instance.getCarRegos().get(rid);
		if (rego == null) {
			msg.put("msg", "GET: Car registration with " + rid +  " not found");
			return Response.status(404).entity(msg).build();
		}
		
		//TODO: may need to add more values to the JSON
		JSONObject noticeJSON = new JSONObject();
		noticeJSON.put("lastName", rego.getLastName());
		noticeJSON.put("firstName", rego.getFirstName());
		noticeJSON.put("licenseNumber", rego.getLicenseNum());
		noticeJSON.put("registrationNumber", rego.getRegoNum());
		noticeJSON.put("_rid", rego.get_rid());
		logger.info("got noticeJSON: " + noticeJSON);
		
		return Response.ok(noticeJSON, MediaType.APPLICATION_JSON).build();
	}
	
	//HTTP PUT to update the car registration to reflect the fact that it has been renewed
	//(e.g., this can be done by updating last_registered date)
	// make sure when you're using this function, you don't accidentally change any other factors about the CarRego
	@SuppressWarnings("unchecked")
	@PUT
	@Path("{rid}")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response putCarRego(@HeaderParam("Key") String key, CarRego carRego) {
		//only the officer should be able to access the car registrations
		if (key == null || !key.equals(officerKey)) {
			JSONObject msg = new JSONObject();
			msg.put("msg", "You do not have the correct key to access this.");
			return Response.status(401).entity(msg).build();
		}
		
		if (CarRegoDAO.instance.getCarRegos().containsKey(carRego.get_rid())) {
			CarRegoDAO.instance.getCarRegos().put(carRego.get_rid(), carRego);
			return Response.status(200).build();
		} else {
			//Assuming that we shouldn't be creating a new car registration with PUT
			return Response.status(404).entity("PUT: Car registration with " + carRego.get_rid() +  " not found").build();
		}
		
	}
	
	
}
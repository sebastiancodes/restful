package au.edu.unsw.soacourse.pinkslip;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.math.BigInteger;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.jws.WebService;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;

import au.edu.unsw.soacourse.slipdefinitions.*;

@WebService(endpointInterface = "au.edu.unsw.soacourse.pinkslip.PinkSlipService")
public class PinkSlipServiceImpl implements PinkSlipService {
	ObjectFactory factory = new ObjectFactory();
	String msg, code;
	ServiceFaultType fault = factory.createServiceFaultType();
	
	
	// create a directory under catalina home for the database stuff
	String dir = System.getProperty("catalina.home");
	String directoryName = dir + "/webapps/ROOT/9322Databases";
	File directory = new File(directoryName);
	
	String databasePath = directoryName + "/PinkSlip_Database.xml";
	String queryResult = directoryName + "/PinkSlip_result.xml";
	
	public SafetyCheckOutput safetyCheck(SlipInput parameters)
			throws SafetyCheckFaultMsg {
		
		setUpDirectory();
		
		String safetyCheckQuery = directoryName + "/PinkSlip_safetyCheck.xsl";
		
		SafetyCheckOutput output = factory.createSafetyCheckOutput();
		
		try {

			TransformerFactory tFactory = TransformerFactory.newInstance();

			Transformer transformer = tFactory
					.newTransformer(new javax.xml.transform.stream.StreamSource(
							safetyCheckQuery));

			transformer.setParameter("fName", parameters.getFirstName());
			transformer.setParameter("lName", parameters.getLastName());
			transformer.setParameter("regoNum", parameters.getRegoNum());
			
			transformer.transform(new javax.xml.transform.stream.StreamSource(
					databasePath), new javax.xml.transform.stream.StreamResult(
					new FileOutputStream(queryResult)));
			
			Scanner sc = new Scanner(new File(queryResult));
			if(sc.hasNext()) {
				String xmlResult = sc.nextLine();
				if (xmlResult.contains("<answer>Yes</answer>")) {
					output.setSafetyCheckFlag(true);
				} else if (xmlResult.contains("<answer>No</answer>")) {
					output.setSafetyCheckFlag(false);
				} else { //means driver couldn't be found
					msg = "Driver doesn't exist in the database";
					code = "ERR_DRIVER_NOT_FOUND";
					fault.setErrtext(msg);
					fault.setErrcode(code);
					sc.close();
					throw new SafetyCheckFaultMsg(msg, fault);
				}
			}
			sc.close();
			
		} catch (Exception e) {
			msg = "Technical error (couldn't run query on database)";
			code = "ERR_XML_TRANSFORM";
			fault.setErrtext(msg);
			fault.setErrcode(code);
			throw new SafetyCheckFaultMsg(msg, fault);
		}
		
		return output;
	}

	public VehicleAgeOutput vehicleAge(GreenSlipOutput parameters)
			throws VehicleAgeFaultMsg {
		setUpDirectory();
		String vehicleAgeQuery = directoryName + "/PinkSlip_vehicleAge.xsl";
		System.out.println("vehicleAgeQuery is at " + vehicleAgeQuery);

		File file = new File(vehicleAgeQuery);
		if (!file.exists()) {
			System.out.println("can't find " + vehicleAgeQuery);
		}
		
		VehicleAgeOutput output = factory.createVehicleAgeOutput();
		
		try {

			TransformerFactory tFactory = TransformerFactory.newInstance();

			Transformer transformer = tFactory
					.newTransformer(new javax.xml.transform.stream.StreamSource(
							vehicleAgeQuery));
			
			transformer.setParameter("fName", parameters.getFirstName());
			transformer.setParameter("lName", parameters.getLastName());
			transformer.setParameter("regoNum", parameters.getRegoNum());
			
			transformer.transform(new javax.xml.transform.stream.StreamSource(
					databasePath), new javax.xml.transform.stream.StreamResult(
					new FileOutputStream(queryResult)));
			
		} catch (Exception e) {
			msg = "Technical error (couldn't run query on database)";
			code = "ERR_XML_TRANSFORM";
			fault.setErrtext(msg);
			fault.setErrcode(code);
			throw new VehicleAgeFaultMsg(msg, fault);
		}
		
		Scanner sc;
		try {
			sc = new Scanner(new File(queryResult));
			if(sc.hasNext()) {
				String xmlResult = sc.nextLine();
				Pattern p = Pattern.compile("<answer>([0-9]{4})</answer>");
				Matcher m = p.matcher(xmlResult);
				if (m.find()) {
					//m.group(0) is just the whole text
					int manufacturedYear = Integer.parseInt(m.group(1));
					output.setAge(BigInteger.valueOf(2015 - manufacturedYear));
				} else { //means driver couldn't be found
					msg = "Driver doesn't exist in the database";
					code = "ERR_DRIVER_NOT_FOUND";
					fault.setErrtext(msg);
					fault.setErrcode(code);
					sc.close();
					throw new VehicleAgeFaultMsg(msg, fault);
				}
			}
			sc.close();
		} catch (FileNotFoundException e) {
			msg = "Technical error (couldn't open xml result from querying database";
			code = "ERR_SCANNER";
			fault.setErrtext(msg);
			fault.setErrcode(code);
			throw new VehicleAgeFaultMsg(msg, fault);
		}
		
		
		return output;
	}
	
	private void setUpDirectory() {
		File directory = new File(directoryName);
		if (!directory.exists()) {
			directory.mkdir();
		}
		System.out.println(directoryName);
	}

}

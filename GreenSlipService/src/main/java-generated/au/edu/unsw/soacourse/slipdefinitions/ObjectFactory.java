
package au.edu.unsw.soacourse.slipdefinitions;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the au.edu.unsw.soacourse.slipdefinitions package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _SlipFault_QNAME = new QName("http://soacourse.unsw.edu.au/SlipDefinitions", "slipFault");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: au.edu.unsw.soacourse.slipdefinitions
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link SlipInput }
     * 
     */
    public SlipInput createSlipInput() {
        return new SlipInput();
    }

    /**
     * Create an instance of {@link GreenSlipOutput }
     * 
     */
    public GreenSlipOutput createGreenSlipOutput() {
        return new GreenSlipOutput();
    }

    /**
     * Create an instance of {@link SafetyCheckOutput }
     * 
     */
    public SafetyCheckOutput createSafetyCheckOutput() {
        return new SafetyCheckOutput();
    }

    /**
     * Create an instance of {@link VehicleAgeOutput }
     * 
     */
    public VehicleAgeOutput createVehicleAgeOutput() {
        return new VehicleAgeOutput();
    }

    /**
     * Create an instance of {@link ServiceFaultType }
     * 
     */
    public ServiceFaultType createServiceFaultType() {
        return new ServiceFaultType();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ServiceFaultType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soacourse.unsw.edu.au/SlipDefinitions", name = "slipFault")
    public JAXBElement<ServiceFaultType> createSlipFault(ServiceFaultType value) {
        return new JAXBElement<ServiceFaultType>(_SlipFault_QNAME, ServiceFaultType.class, null, value);
    }

}

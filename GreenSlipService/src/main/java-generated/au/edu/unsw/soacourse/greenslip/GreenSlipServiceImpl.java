package au.edu.unsw.soacourse.greenslip;

import java.io.File;
import java.io.FileOutputStream;
import java.util.Scanner;

import javax.jws.WebService;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;

import au.edu.unsw.soacourse.slipdefinitions.*;

@WebService(endpointInterface = "au.edu.unsw.soacourse.greenslip.GreenSlipService")
public class GreenSlipServiceImpl implements GreenSlipService {

	ObjectFactory factory = new ObjectFactory();

	public GreenSlipOutput greenSlipCheck(SlipInput parameters)
			throws GreenSlipFaultMsg {

		String msg, code;
		ServiceFaultType fault = factory.createServiceFaultType();
		// check parameters here and return appropriate result

		String lName = parameters.getLastName();
		String fName = parameters.getFirstName();
		String regoNum = parameters.getRegoNum();

		// create a directory under catalina home for the database stuff
		String dir = System.getProperty("catalina.home");
		String directoryName = dir + "/webapps/ROOT/9322Databases";
		File directory = new File(directoryName);
		if (!directory.exists()) {
			directory.mkdir();
		}
		System.out.println(directoryName);
		
		String databasePath = directoryName + "/GreenSlip_Database.xml";
		String queryPath = directoryName + "/GreenSlip_selectDriver.xsl";
		String queryResult = directoryName + "/GreenSlip_result.xml";

		GreenSlipOutput output = factory.createGreenSlipOutput();

		try {

			TransformerFactory tFactory = TransformerFactory.newInstance();

			File database = new File(databasePath);
			if (!database.exists()) {
				System.out.println("can't find " + databasePath);
			}
			
			Transformer transformer = tFactory
					.newTransformer(new javax.xml.transform.stream.StreamSource(
							queryPath));

			transformer.setParameter("fName", fName);
			transformer.setParameter("lName", lName);
			transformer.setParameter("regoNum", regoNum);

			transformer.transform(new javax.xml.transform.stream.StreamSource(
					databasePath), new javax.xml.transform.stream.StreamResult(
					new FileOutputStream(queryResult)));

			Scanner sc = new Scanner(new File(queryResult));
			if (sc.hasNext()) {
				String xmlResult = sc.nextLine();
				if (xmlResult.contains("<answer>Yes</answer>")) {
					output.setPaidFlag(true);
				} else if (xmlResult.contains("<answer>No</answer>")) {
					output.setPaidFlag(false);
				} else { // means driver couldn't be found
					msg = "Driver doesn't exist in the database";
					code = "ERR_DRIVER_NOT_FOUND";
					fault.setErrtext(msg);
					fault.setErrcode(code);
					sc.close();
					throw new GreenSlipFaultMsg(msg, fault);
				}
			}
			sc.close();

		} catch (Exception e) {
			msg = "Technical error (couldn't run query on database)";
			code = "ERR_XML_TRANSFORM";
			fault.setErrtext(msg);
			fault.setErrcode(code);
			throw new GreenSlipFaultMsg(msg, fault);
		}

		return output;
	}

}
